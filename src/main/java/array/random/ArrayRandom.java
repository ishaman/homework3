package array.random;

public class ArrayRandom {
    public static void main(String[] args) {
        int[] ins = new int[15];
        int count = 0;
        System.out.println("Исходный массив из 15 элементов: " + "\n");
        for (int i = 0; i < ins.length; i++) {
            ins[i] = -1984 + (int)(Math.random()*((105 + 1984)+1));
            if (ins[i]%2 == 0){
                count++;
            }
        }
        for (int i = 0; i < ins.length; i++) {
            System.out.println(ins[i]);
        }
        System.out.println("\n");
        System.out.println("Из них четных: " + count);
        System.out.println("Result array: " + "\n");
        int[] chet = new int[count];
        int j = 0;
        for (int i = 0; i < ins.length; i++) {
            if (ins[i]%2 == 0){
                chet[j] = ins[i];
                j++;
            }
        }
        for (int i = 0; i < chet.length; i++) {
            System.out.println(chet[i]);
        }

    }

}
